<?php

include 'lib353pdo.php';
include('logininfo.php');

$db = connect_pdo($hostname, $username, $password, $dbname);
print 	"<html><title>Updating Employees</title><body>\n";
if ($_POST['submit_ssn']) {		
	$ssn=$_POST['ssn'];
	printform2($db, $ssn);
} else if ($_POST['update']) {		
	$ssn=$_POST['ssn'];
	print "updating employee with SSN=$ssn<p>";
	update_employee($db, $ssn);
	printform1($db);

} else {				
	printform1($db);
}
print	"</body></html>";




function get_employees($db) {
	$query="select e.fname, e.lname, e.ssn, 
	concat(d.dnumber, ' (', d.dname, ')' ) as dept
	from (employee e left join employee s on e.super_ssn = s.ssn) 
	left outer join department d on e.dno = d.dnumber";

	$qstmt = $db->prepare($query);  
	if ($qstmt == FALSE) {
		print("failed to prepare statement: \"$query\"<p>\n");
		$errarray = $db->errorInfo();
		$errmsg = $errarray[2];  
		print("<b>Prepare error: $errmsg</b><p>\n");
		die();
	}

	$ret = $qstmt->execute();

	if ($ret == FALSE) {
		print("query not successful: \"$query\"<p>\n");
		$errarray = $qstmt->errorInfo();
		$errmsg = $errarray[2];  
		print("<b>Execute error: $errmsg</b><p>\n");
		die();
	}

	print "<h3>Table of Employees</h3>";
	table_format_pdo($qstmt);
	print "<p>";
}


function printform1($db) {
print <<<FORMEND
	<form method="post" action="">
	Use this page to select an employee ssn<p>
	<input type="text" name="ssn" value="123456789">
        <p><input type="submit" name="submit_ssn" value="submit ssn">
	</form>
	<p>
FORMEND;
	get_employees($db);
	makePageButtons();
}


function update_employee($db, $orig_ssn) {
    $fname= $_POST['fname'];
	$minit= $_POST['minit'];
	$lname= $_POST['lname'];
	$ssn  = $_POST['ssn'];
	$bdate= $_POST['bdate'];
	$address=$_POST['address'];
	$sex  = $_POST['sex'];
	$salary=$_POST['salary'];
	$super_ssn=$_POST['super_ssn'];
	$dno  = $_POST['dno'];

	
	
	$insertion="update employee set fname=?, minit=?, lname=?, bdate=?, address=?, sex=?, salary=?, super_ssn=?, dno=? where ssn=$orig_ssn";

	$types = array('text', 'text', 'text','text', 'text', 'text','decimal', 'text', 'integer');	

	
	$stmt = $db->prepare($insertion);
	
	

	$queryargs = array($fname, $minit, $lname, $bdate, $address,
		$sex, $salary, $super_ssn, $dno);

	

	$ires = $stmt->execute($queryargs);

	
}


function printform2($db, $ssn) {
	$query="select e.fname, e.minit, e.lname, e.ssn, e.bdate, e.address, e.sex, e.super_ssn, e.salary,
		concat(d.dnumber, ' (', d.dname, ')' ) as dept, d.dnumber as deptNum
		from (employee e left join employee s on e.super_ssn = s.ssn) 
		left outer join department d on e.dno = d.dnumber
		where e.ssn=$ssn";
	$qstmt = $db->prepare($query);
	if ($qstmt == FALSE) {
		print("failed to prepare statement: \"$query\"<p>\n");
		$errarray = $db->errorInfo();
		$errmsg = $errarray[2];  
		print("<b>Prepare error: $errmsg</b><p>\n");
		die();
	}
	
	$qstmt->execute();
	$mappedrow = $qstmt->fetch();
	
    		//print_r($mappedrow); Testing it out
  	
	
	
	$qstmt->execute();
	
	

	
	
	print "<h3>This is the current information stored in the database</h3>";
	table_format_pdo($qstmt);
	print "<p>";

	print "<h3>Since prepared statements could not be bound with fetch_array like direct mysql, I could not build this</h3>";
    	
	print <<<END
	<b>This form is for updating the employee with ssn $ssn</b><p>
	<form method="post" action="">
	<input type="text" name="ssn" value="$ssn" readonly> SSN of employee<p>
	<br><input type="text" name="fname" value="$mappedrow[fname]"> First Name
	<br><input type="text" name="minit" value="$mappedrow[minit]"> Middile Initial
	<br><input type="text" name="lname" value="$mappedrow[lname]"> Last Name
	<br><input type="text" name="bdate" value="$mappedrow[bdate]"> Birth Date
	<br><input type="text" name="address" value="$mappedrow[address]"> Address
	<br><input type="text" name="sex" value="$mappedrow[sex]"> Sex
	<br><input type="text" name="salary" value="$mappedrow[salary]"> Salary
	<br><input type="decimal" name="dno" value="$mappedrow[deptNum]"> Department Number
	<br><input type="text" name="super_ssn" value="$mappedrow[super_ssn]"> Supervisor SSN
	<p><input type="submit" name="update" value="Update">
	</form>
END;

	makePageButtons();
}
?>


