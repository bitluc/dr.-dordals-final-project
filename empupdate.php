<?php

include 'lib353pdo.php';

/*
 * In employees2.php, we display the existing employee table 
 * and then provide a form for adding a new employee.
 * 
 * prepared statements are used!
 * filtering:  
 *	htmlspecialchars() for html filtering
 * (except I don't actually use that)
 *
 * This file generates two html forms, form1 and form2
 */


// main program here
// decide which button was clicked: 'submit' or 'update'


include 'password.php';
$db = connect_pdo($hostname, $username, $password, $dbname);
if ($db==null) {
	die("connect to DB failed!");
}

print 	"<html><title>Updating Employees</title><body>\n";
if ($_POST['submit_ssn']) {		// came from form1
	$ssn=$_POST['ssn'];
	printform2($db, $ssn);
} else if ($_POST['update']) {		// came from form2
	$ssn=$_POST['ssn'];
	print "updating employee with SSN=$ssn<p>";
	update_employee($db, $ssn);
	printform1($db);
} else {				// first arrival
	printform1($db);
}
print	"</body></html>";
// end of main program


/*
 * This is mostly just copied from employee.php. 
 */
function update_employee($db, $orig_ssn) {
      	$fname= $_POST['fname'];
	$minit= $_POST['minit'];
	$lname= $_POST['lname'];
	$ssn  = $_POST['ssn'];
	$bdate= $_POST['bdate'];
	$address=$_POST['address'];
	$sex  = $_POST['sex'];
	$salary=$_POST['salary'];
	$super_ssn=$_POST['super_ssn'];
	$dno  = $_POST['dno'];

	// diagnostics
	print htmlspecialchars("inserting record: lname=$lname, fname=$fname, ssn=$ssn, fssn=$fssn <p>");

	// WRONG INSERTION FOR THIS PAGE!!! CHANGE TO AN APPROPRIATE UPDATE!!
	// something like "update employee set .... where ssn=$orig_ssn" (but use prepared query)
	$insertion="insert into EMPLOYEE values (?,?,?,?,?,?,?,?,?,?)";

	$types = array('text', 'text', 'text', 'text', 'text', 'text', 'text',
			'decimal', 	// salary
			'text', 
			'integer');	// dept number

	// change MANIP to RESULT on lamp.cslabs.luc.edu
	$stmt = $db->prepare($insertion);

	if ($stmt == FALSE) {
		print("bad prepared statement in update_employee()");
		die();
	}

	$queryargs = array($fname, $minit, $lname, $ssn, $bdate, $address,
		$sex, $salary, $super_ssn, $dno);

	$ret = $stmt->execute($queryargs);

	if ($ret == FALSE) {
		print("update not successful in update_employee() " );
		$fail=1;
	} else {
		print "update ok";
	}
}

// despite the name, get_employees() also prints the employee table
function get_employees($db) {
	$query="select e.fname, e.lname, e.ssn, 
	concat(d.dnumber, ' (', d.dname, ')' ) as dept
	from (employee e left join employee s on e.super_ssn = s.ssn) 
	left outer join department d on e.dno = d.dnumber";

	$qstmt = $db->prepare($query);
	$ret = $qstmt->execute();

	if ($ret == FALSE) {
		die("query not successful in get_employees()");
	}

	print "<h3>Table of Employees</h3>";
	table_format_pdo($qstmt);
	print "<p>";
}


/*
 * This form is mostly ok
 */
function printform1($db) {
print <<<FORMEND
	<h4>Form 1</h4>
	Use this page to select an employee ssn<p>
	<form method="post" action="">
	<input type="text" name="ssn" value="123456789">
        <p><input type="submit" name="submit_ssn" value="submit ssn">
	</form>
	<p>
FORMEND;
	get_employees($db);
	makePageButtons();
}

/*
 * You will have to add lots of input boxes to this form.
 * You will also have to add code to retrieve the fields for the given employee
 * and use those as the corresponding value attributes of the input boxes
 * Use the "$lname" example as a pattern; you will have retrieved a value for $lname with a query
 */
function printform2($db, $ssn) {
	$lname = "retrieve this from db with a query!!";
print <<<END
	<h4>Form 2</h4>
	<b>This form is for updating the employee with ssn $ssn</b><p>
	<form method="post" action="">
	<input type="text" name="ssn" value="$ssn" readonly> SSN of employee<p>
	<input type="text" name="lname" size="30" value="$lname">
	</form>
END;
	makePageButtons();
}
?>
