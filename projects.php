<?php
/*
 * In employees.php, we display the existing employee table
 * and then provide a form for adding a new employee.
 *
 * prepared statements are used!
 * filtering: 
 *    htmlspecialchars() for html filtering
 */

include 'lib353pdo.php';

// main program here
// decide which button was clicked: 'submit' or 'update'
include('logininfo.php');

$db = connect_pdo($hostname, $username, $password, $dbname);



print     "<html><title>Adding Projects</title><body>\n";
if (array_key_exists('submit', $_POST)) {      
    insert_project($db);
    get_projects($db);
} else if ($_POST['update']) {    
    get_projects($db);

} else {             
    get_projects($db);
}
printform();   
print    "</body></html>";


function insert_project($db) {
    
    print ("starting insert_project()<br>\n");
    $projectName = $_POST['projectname'];
    $projectLocation = $_POST['projectlocation'];
    $projectNum = $_POST['projectnum'];
          $dno  = $_POST['dno'];

    
    print htmlspecialchars("inserting record: lname=$lname, fname=$fname, ssn=$ssn, fssn=$fssn") . "<p>";

    $insertion="insert into project(pname, plocation, dnum, pnumber) values (?, ?, ?, ?)";

    

    $stmt = $db->prepare($insertion);

    if ($stmt == FALSE) {
        print("failed to prepare statement: \"$insertion\"<p>\n");
        $errarray = $db->errorInfo();
        $errmsg = $errarray[2]; 
        print("<b>Prepare error: $errmsg</b><p>\n");
        die();
    }

    $queryargs = array($projectName, $projectLocation, $dno, $projectNum);

    $ret = $stmt->execute($queryargs);

    if ($ret == FALSE) {
        print("execution of query not successful: \"$insertion\"<p>\n");
        $errarray = $stmt->errorInfo();
        $errmsg = $errarray[2]; 
        print("<b>Execute error: $errmsg</b><p>\n");
        $fail=1;
    } else {
        print "record was inserted<p>";
        $stmt->closeCursor();
    }
}


function get_projects($db) {
    $query="SELECT p.pnumber, p.pname as ProjectName, p.plocation as ProjectLocation, d.dname as DepartmentName, d.dnumber from project as p inner join department as d on p.dnum = d.dnumber";

    $qstmt = $db->prepare($query);   
    if ($qstmt == FALSE) {
        print("failed to prepare statement: \"$query\"<p>\n");
        $errarray = $db->errorInfo();
        $errmsg = $errarray[2]; 
        print("<b>Prepare error: $errmsg</b><p>\n");
        die();
    }

    $ret = $qstmt->execute();

    if ($ret == FALSE) {
        print("query not successful: \"$query\"<p>\n");
        $errarray = $qstmt->errorInfo();
        $errmsg = $errarray[2]; 
        print("<b>Execute error: $errmsg</b><p>\n");
        die();
    }

    print "<h3>Table of Projects</h3>";
    table_format_pdo($qstmt);
    print "<p>";
}



function printform() {
print <<<FORMEND
    <form method="post" action="">
    Use this page to enter new Projects<p>
    <input type="text" name="projectnum">: Project Num<br />   
    <input type="text" name="projectname">: Project Name<br />
    <input type="text" name="projectlocation"> :Project Location<br />
    <p>
    <input type="text" name="dno">: Department Number
    <p><input type="submit" name="submit" value="submit data">
    </form>
FORMEND;
    makePageButtons();
}


?>

